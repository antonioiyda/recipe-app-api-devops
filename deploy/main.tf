#
# The deploy directory has files to make the provision and configuration of project's environments
# .gitlab-ci.yml has the terraform calls and environment setup that are triggered based on
# gitlab branch commits and merge-requests in such a way to streamline the CI/CD process.
#
#     feature/<feature_name> is the developing environment
#     master branch is the staging environment
#     production branch is the prod environment
#
# This project is composed of the following terraform files:
#
#  main.rf = 
#        Terraform backend configuration bucket/lock table
#        AWS provider
#        locals (local variables for the project)
#        data   (dynamic value variables)
#
#  variables.tf
#        Global variables
#  network.tf
#     VPC, subnets, and networking components
#  bastion.tf
#     bastion host provision
#  database.tf
#     provision of database instance
#  output.tf
#     show in the logs of CI/CD    
#
########################################################
#  Commands to format and validate terraform synthax
########################################################
#
#  format the tf file (from root of the project)
#  $ docker-compose -f deploy/docker-compose.yml run --rm terraform fmt
#
#  Validate terraform syntax
#  $ docker-compose -f deploy/docker-compose.yml run --rm terraform validate 
#
#  plan - terraform plan
####################################################################
#  aws-vault - configure aws session token
####################################################################
#
# aws-vault exec <profile> --duration=xxh cmd cmd.exe
#

terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-iyda-tfstate"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devices-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}
