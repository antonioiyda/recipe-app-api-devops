variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "iydaantonio56@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image API URL"
  default     = "751744078213.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for Proxy - URL"
  default     = "751744078213.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

# Set up in the gitlab
variable "django_secret_key" {
  description = "Secret key for Django APP"
}



