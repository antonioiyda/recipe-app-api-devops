# Recipe App API DevOps Starting Point

Source code for my Udemy course Build a [Backend REST API with Python & Django - Advanced](http://udemy.com/django-python-advanced/).

The course teaches how to build a fully functioning REST API using:

 - Python
 - Django / Django-REST-Framework
 - Docker / Docker-Compose
 - Test Driven Development

## Getting started

To start project, run:

```
docker-compose up
```

The API will then be available at http://127.0.0.1:8000

### Lesson 1 - Introduction
    How to create a production grade deployment for a production ready REST API
    Three different environments: Dev, Stage, Production

    Recipe API
        Back-end REST API for recipe management app
            User-registration
            Authentication
            Managing tags, ingredients, recipes
            Uploading images
        
        Django RESP framework within docker containers

### Lesson 2 - Set up Dev environment
    Install and test dependencies
        VS Code (Docker extension, Terraform - Anton Kulikov), Docker desktop, aws-vault, git, ModHeader,
    Setup Gitlab account and SSH

### Lesson 3 - Set up AWS
    New Account, create a new user (admin), add MFA policy to account for security, set up AWS vault,
    Set up Budget in AWS.

### Lesson 4 - Set up NGINX proxy
    NGINX proxy server for static content (Images, JavaScript, CSS)

1. Add a new project in Gitlab for proxy application
2. Create a dockerfile + NGINX configurations
3. Setup AWS account with new ECR repo
4. Create a CI user account for Gitlab to authenticate with AWS
5. Setup pipeline jobs in Gitlab to run workflow
    
### Lesson 5 - Prepare recipe app project for deployment

1. Create a new project in Gitlab to store API code
2. Add uWSGI as dependency
3. Updata dockerfile to use uWSGI server to run application
4. Make changes to the configuration of our project to serve static files through proxy
5. Update configuration to pull settings through environment variables
6. Test that project and proxy work together on local machine

### Lesson 6 - Setup terraform

1. Add AWS resources Terraform needs to function (S3 bucket, DynamoDB table state locking, ECR)
2. Set up project to run Terraform using Docker compose
3. Configure Terraform in our project to use the AWS resources
4. Run Terraform initialize command to initialize Terraform on our local machine
5. Create simple Hello World example with EC2 (will be used later on as a bastion host)
6. Create new workspaces to separate environments
7. Add local prefix to AWS resources
8. Modify gitignore file to exclude files
 
### Lesson 7 - Setup Gitlab CI/CD

1. Set up skeleton configuration for GitLab CI/CD pipelines
2. Test skeleton file to ensure it works
3. Create real jobs for deployment
4. Final test to ensure new jobs are working
5. New CI user in IAM to enable authentication

### Lesson 8 - Configure network

1. Create VPC, subnets, IGW, NAT-GTW, routing tables, SG.

### Lesson 9 - Create database

1. Modify CI user permissions so Terraform can create and destroy database
2. Define database with RDS in Terraform
3. Add variables to Gitlab project to configure username and password for database

### Lesson 10 - Update bastion

1. Modify CI user so Terraform has permission to update bastion
2. Add SSH key to authenticate with bastion server from local machine
3. Add user data script to install dependencies on bastion server for administration
4. Add instance profile to bastion server to access resources in AWS (ECR)
5. Add bastion server to the public subnet
6. Add Terraform output to determine bastion hostname

### Lesson 11 - Setup ECS
1. Create ECS components to group components
2. Define a task role to give ECS access to other AWS resources
    1 role to start the service
    2 permissions to the running service itself
3. Add a cloud watch log group to keep log outputs for containers in one place
4. Add container definition to define how containers are run in ECS (CPU, memory)
    Image registry and image tag
    Memory to assign to it
    Different configuration options - env. variables

5. Create task definition to assign to ECS service
    Define our task definition within our ECS Terraform
    
6. Deploy the infrastructure + inspect CloudWatch logs to ensure containers are running correctly

